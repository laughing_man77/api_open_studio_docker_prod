# naala89/apiopenstudio

This image provides a docker image for production use ApiOpenStudio.

It contains:

* Nginx
* PHP 8.1
* The tagged version of ApiOpenStudio
* composer

## Getting Started

This container contains a full working Nginx/PHP server and ApiOpenStudio. It
requires a separate MariaDB container, and can be quickly installed on a
bare-bones production server in minutes to have a fully functioning
ApiOpenStudio instance.

**Note:** There should not be a running instance of Apache or Nginx on the
server, otherwise the incoming port 80/443 requests will not get directed to
the Docker container.

## Prerequisites

In order to run this container you'll need docker installed.

* [Windows][docker_windows]
* [OS X][docker_osx]
* [Linux][docker_linux]

## Usage

## Useful File Locations

* `/etc/nginx/nginx.conf` - The main Nginx config file.
* `/etc/nginx/sites-enabled/default` - The Nginx site config file.
* `/etc/php/8.1/{cli,fpm}/php.ini` - PHP configuration.
* `/var/www/html/install.sh` - Install composer, npm and gulp dependencies.

## Example

```shell
# Create the database volume
sudo docker volume create dbdata

# Create the Docker network
sudo docker network create api_network

# Start the database container
sudo docker run -d --name=apiopenstudio-db \
-e MYSQL_DATABASE=apiopenstudio \
-e MYSQL_USER=apiopenstudio \
-e MYSQL_PASSWORD=my_s3cr3t \
-e MYSQL_ROOT_PASSWORD=my_s3cr3t \
-h apiopenstudio-db \
-v dbdata:/var/lib/mysql \
--restart=unless-stopped \
--network=api_network \
mariadb:latest

# Download the settings file.
sudo mkdir -p /data/apiopenstudio
sudo curl https://raw.githubusercontent.com/naala89/apiopenstudio/master/example.docker.settings.yml -o /data/apiopenstudio/settings.yml

# Required updated settings.yml changes
* db.root_password
* db.username
* db.password
* api.url
* api.jwt_issuer
* api.jwt_permitted_for

# Start the API container
# Replace the following with the correct values:
# /path/to/ssl.crt, /path/to/ssl.key, naala89/apiopenstudio:tag
sudo docker run -d --name apiopenstudio-api \
-p 80:80 \
-p 443:443 \
--mount type=bind,source=/data/apiopenstudio/settings.yml,target=/var/www/html/settings.yml \
--mount type=bind,source=/path/to/ssl.crt,target=/etc/nginx/certs/apiopenstudio.crt \
--mount type=bind,source=/path/to/ssl.key,target=/etc/nginx/certs/apiopenstudio.key \
--network=api_network \
--restart=unless-stopped \
naala89/apiopenstudio:tag

# SSH into the API container
sudo docker exec -it apiopenstudio-api bash

# Install composer dependencies and database.
./install.sh
```

## Detailed instructions

Visit the [ApiOpenStudio wiki][wiki] for full instructions and configuration of
`settings.yml`

# Find Us

* [GitLab][gitlab_apiopenstudio]
* [GitHub][github_naala89]
* [www.apiopenstudio.com][web_apiopenstudio]

# Versioning

This image automatically built from tags on the master branch at
[GitLab][gitlab_apiopenstudio].

Version tags (i.e. `naala89/apiopenstudio:1.0`) references the version of the
image and the tag of the release.

Version tags of `master` & `develop` (i.e. `naala89/apiopenstudio:master`)
references the latest version on the branch.

The `latest` tag is most recent built image and should not be used to pull the
latest tagged version.

# Image Developers

## Uploading with Pipelines

### Required `CI/CD` Variables:

* `CI_REPOSITORY` - The path to the repository to build from,
  i.e. `https://gitlab.com/naala89/apiopenstudio`
* `SSH_PRIVATE_KEY` - Any valid SSH private key
* `CI_REGISTRY` - `docker.io`
* `CI_IMAGE_NAME` - The name of the image to be uploaded to docker,
  i.e. `apiopenstudio`
* `CI_REGISTRY_USER` - Docker Hub username
* `CI_REGISTRY_PASSWORD` - Docker Hub password

### Instructions

* Log into GitLab
* Navigate to CI/CD -> Pipelines
* Click on `Run pipeline`
* `Run for branch name or tag`: Select `master`
  (this is the branch to run the build pipelines from)
* Variables to upload a Branch:
    * `BRANCH` - Branch name
* Variables to upload a TAG:
    * `TAG` - Tag name

## Building and uploading locally

### `.env`

Add a `.env` file to your clone of this repo. Contents:

```dotenv
CI_REGISTRY=docker.io
CI_REPOSITORY=https://gitlab.com/<user_name>/<repo_name>
CI_IMAGE_NAME=apiopenstudio
CI_REGISTRY_USER=<docker_username>
CI_REGISTRY_PASSWORD=<docker_password>
```

### Run build & upload command:

``` shell
# Upload an image of branch_name
./upload.sh -b branch_name

# Upload an image of tag_name
./upload.sh -t tag_name
```

### Parameters in the upload script

You must declare either `-b` or `-t` parameters, to specify what version of
ApiOpenStudio Admin to publish and tag.

All other parameters are optional and will override the `.env` file.

* `-b` Repository branch
* `-t` Repository tag
* `-r` Repository SSH clone path
* `-i` Docker Hub Image name
* `-u` Docker user name
* `-p` Docker user password
* `-h` Command help

# Authors

* **John Avery** - [John - GitLab][gitlab_john]

# License

This project is licensed under the MIT License - see the [LICENSE.md][license]
file for details.

[docker_windows]: https://docs.docker.com/desktop/install/windows-install/

[docker_osx]: https://docs.docker.com/desktop/install/mac-install/

[docker_linux]: https://docs.docker.com/desktop/install/linux-install/

[gitlab_apiopenstudio]: https://gitlab.com/apiopenstudio/apiopenstudio

[gitlab_john]: https://gitlab.com/john89

[github_naala89]: https://github.com/naala89

[web_apiopenstudio]: https://www.apiopenstudio.com

[license]: https://gitlab.com/apiopenstudio/docker_images/apiopenstudio_docker_prod/-/blob/master/LICENSE.md

[wiki]: https://wiki.apiopenstudio.com/install/docker/production/core
