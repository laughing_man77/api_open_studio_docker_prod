#!/usr/bin/env bash

# Set fonts for Help.
NORM=`tput sgr0`
BOLD=`tput bold`
REV=`tput smso`

# Set Script Name variable
SCRIPT=$(basename ${BASH_SOURCE[0]})

# Import the .env file.
set -o allexport
source .env
set +o allexport

# Help function
function HELP {
  echo -e \\n"Help documentation for ${BOLD}${SCRIPT}.${NORM}"\\n
  echo -e "${REV}Basic usage:${NORM} ${BOLD}${SCRIPT} -t tag-name${NORM}"\\n
  echo "Command line switches are optional. The following switches are recognized."
  echo "${REV}-b${NORM}  --Sets the value for option ${BOLD}BRANCH${NORM}."
  echo "${REV}-t${NORM}  --Sets the value for option ${BOLD}TAG${NORM}."
  echo "${REV}-r${NORM}  --Sets the value for option ${BOLD}CI_REPOSITORY${NORM}."
  echo "${REV}-i${NORM}  --Sets the value for option ${BOLD}CI_IMAGE_NAME${NORM}."
  echo "${REV}-u${NORM}  --Sets the value for option ${BOLD}CI_REGISTRY_USER${NORM}."
  echo "${REV}-p${NORM}  --Sets the value for option ${BOLD}CI_REGISTRY_PASSWORD${NORM}."
  echo -e "${REV}-h${NORM}  --Displays this help message. No further functions are performed."\\n
  echo -e "Example: ${BOLD}${SCRIPT} -b master${NORM} - build and upload the latest from the master branch"
  echo -e "Example: ${BOLD}${SCRIPT} -t 1.0.1${NORM} - build and upload the tag 1.0.1"
  exit 1
}

while getopts b:t:r:i:h flag; do
  case "${flag}" in
    b)
      BRANCH=${OPTARG}
      ;;
    t)
      TAG=${OPTARG}
      ;;
    r)
      CI_REPOSITORY=${OPTARG}
      ;;
    i)
      CI_IMAGE_NAME=${OPTARG}
      ;;
    u)
      CI_REGISTRY_USER=${OPTARG}
      ;;
    p)
      CI_REGISTRY_PASSWORD=${OPTARG}
      ;;
    h)
      HELP
      ;;
    \?)
      echo -e \\n"Option -${BOLD}${OPTARG}${NORM} not allowed."
      HELP
      exit 1
      ;;
    esac
done

if [[ -z ${BRANCH} ]] && [[ -z ${TAG} ]]; then
  echo -e \\n"You must specify a branch or tag (options: ${BOLD}-b${NORM} or ${BOLD}-t${NORM})."
  exit 1
fi

echo ${CI_REGISTRY_PASSWORD} | docker login -u ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY}

if [[ ! -z "${BRANCH}" ]]; then
  IMAGE_TAG=${BRANCH}
else
  IMAGE_TAG=${TAG}
fi
IMAGE_TAG=$(echo "${IMAGE_TAG}" | sed -r 's/\//_/g')

docker buildx build -t "${CI_IMAGE_NAME}" \
  --platform linux/amd64 \
  --ssh default \
  --build-arg CI_REPOSITORY="${CI_REPOSITORY}" \
  --build-arg BRANCH="${BRANCH}" \
  --build-arg TAG="${IMAGE_TAG}" .

docker tag "${CI_IMAGE_NAME}" "${CI_REGISTRY_USER}/${CI_IMAGE_NAME}:${IMAGE_TAG}"

docker push "${CI_REGISTRY_USER}/${CI_IMAGE_NAME}:${IMAGE_TAG}"
