ARG UBUNTU_VERSION=22.04

FROM ubuntu:${UBUNTU_VERSION}

LABEL org.opencontainers.image.authors="https://www.apiopenstudio.com" \
    org.opencontainers.image.description="ApiOpenStdio application packaged by ApiOpenStdio" \
    org.opencontainers.image.source="https://gitlab.com/apiopenstudio/docker_images/apiopenstudio_docker_prod" \
    org.opencontainers.image.title="ApiOpenStudio Docker Prod" \
    org.opencontainers.image.vendor="Naala Pty Ltd" \
    org.opencontainers.image.version="1.0.0"

ARG PHP_VERSION=8.1
ARG COMPOSER_VERSION=2.4.4
ARG CI_REPOSITORY=https://github.com/naala89/apiopenstudio
ARG BRANCH
ARG TAG

ENV DEBIAN_FRONTEND noninteractive
ENV PHP_VERSION=${PHP_VERSION}
ENV COMPOSER_VERSION=${COMPOSER_VERSION}
ENV CI_REPOSITORY=${CI_REPOSITORY}
ENV BRANCH=${BRANCH}
ENV TAG=${TAG}

RUN apt-get update \
    && apt-get install -yq \
    curl \
    git \
    mysql-client \
    nginx \
    openssh-client \
    php${PHP_VERSION}-cli \
    php${PHP_VERSION}-curl \
    php${PHP_VERSION}-fpm \
    php${PHP_VERSION}-mysqli \
    php${PHP_VERSION}-xml \
    unzip \
    wget \
    zip

# Install yq
RUN wget https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 -O /usr/local/bin/yq \
    && chmod +x /usr/local/bin/yq

# Install Composer
RUN curl -o /tmp/composer-setup.php https://getcomposer.org/installer \
    && curl -o /tmp/composer-setup.sig https://composer.github.io/installer.sig \
    && php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== trim(file_get_contents('/tmp/composer-setup.sig'))) { unlink('/tmp/composer-setup.php'); echo 'Invalid installer' . PHP_EOL; exit(1); }" \
    && php /tmp/composer-setup.php --no-ansi --install-dir=/usr/local/bin --filename=composer --version=${COMPOSER_VERSION}

# Add application codebase
WORKDIR /tmp
COPY scripts/download.sh .
RUN chmod +x download.sh
RUN echo "${CI_REPOSITORY} ${BRANCH} ${TAG}"
RUN ["/bin/bash", "-c", "./download.sh ${CI_REPOSITORY} ${BRANCH} ${TAG}"]
RUN rm ./download.sh

# Nginx config
COPY config/nginx/nginx.conf /etc/nginx/nginx.conf
COPY config/nginx/sites-available/default /etc/nginx/sites-available/default
RUN mkdir -p /etc/nginx/certs/

WORKDIR /var/www/html

COPY scripts/install.sh .
RUN chmod 700 install.sh

EXPOSE 80
EXPOSE 443

CMD ["/bin/bash", "-c", "/usr/sbin/service php8.1-fpm start && nginx -g 'daemon off;'"]
