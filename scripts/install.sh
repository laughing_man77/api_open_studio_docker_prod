#!/usr/bin/env bash

# Install codebase dependencies.
composer install --no-dev

# Install ApiOpenStudio
php ./bin/aos-install
sleep 2
JWT_PRIVATE_KEY=$(cat settings.yml | yq '.api.jwt_private_key')
JWT_PUBLIC_KEY=$(cat settings.yml | yq '.api.jwt_public_key')
chown www-data $JWT_PRIVATE_KEY $JWT_PUBLIC_KEY
